# -*- coding:utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Blog(models.Model):
	title = models.CharField(u'Название', max_length=50)
	content = models.TextField('Содержание',null=True, blank=True)
	user = models.ForeignKey(User)

	class Meta:
		verbose_name=u'Блог'
		verbose_name_plural = u'Блоги'

	def __unicode__(self):
		return self.title

class Post(models.Model):
	title = models.CharField(u'Название', max_length=50)
	content = models.TextField(u'Содержание',null=True, blank=True)
	blog = models.ForeignKey(Blog)

	created = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.title

	class Meta:
		verbose_name=u'Пост'
		verbose_name_plural = u'Посты'

class Vote(models.Model):
	post = models.ForeignKey(Post)
	votes = models.IntegerField()

class Comments(models.Model):
	content = models.TextField(u'Комментрий', null=True, blank=True)
	comment = models.ForeignKey(Post)
