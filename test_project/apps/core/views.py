from django.shortcuts import HttpResponse, render

from core.models import Post

def main(request, template='main.html'):
	posts = Post.objects.all()
	context = {
	'posts':posts,
	}
	return render(request, template, context)