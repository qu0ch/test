from django.contrib import admin

from .models import Blog, Post, Vote, Comments

class VoteInline(admin.StackedInline):
    model = Vote
    extra = 1

class CommenstInLine(admin.StackedInline):
	model = Comments
	extra = 1

class PostAdmin(admin.ModelAdmin):
	fields = ['title', 'content']
	inlines = [
				VoteInline,
				CommenstInLine
	]

admin.site.register(Blog)
admin.site.register(Post, PostAdmin)